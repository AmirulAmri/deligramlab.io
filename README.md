# Deligram

Deligram system made by Shoppegram.

## How to run

Before run make sure you delete the node_modules file first

1. Then run npm install

npm run watch to Live reload

npm run dev to compile in development mode

npm run build to compile in production mode

## Hot to use/code

1. Edit all of the files inside src folder only.
2. If you make any changes on the css/js files or you upload any images please run npm run dev/npm run build command.
